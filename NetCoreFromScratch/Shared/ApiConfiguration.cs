﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared
{
    public class ApiConfiguration
    {
        public bool Enabled { get; set; }

        public int Timeout { get; set; }
    }
}
