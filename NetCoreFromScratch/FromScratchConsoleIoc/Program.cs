﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace FromScratchConsoleIoc
{
    class Program
    {
        static void Main(string[] args)
        {

            IServiceCollection services = new ServiceCollection();

            IServiceProvider serviceProvider = services
                .AddSingleton(typeof(ApiClass))
                .BuildServiceProvider();

            var ojbFromIoc = serviceProvider.GetService<ApiClass>();


            Console.WriteLine($"Hello, my api key is {ojbFromIoc}");
        }
    }
}
