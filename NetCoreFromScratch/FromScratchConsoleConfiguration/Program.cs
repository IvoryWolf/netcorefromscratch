﻿using Microsoft.Extensions.Configuration;
using System;
using Shared;

namespace FromScratchConsoleConfiguration
{
    class Program
    {
        static void Main(string[] args)
        {
            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder();

            IConfigurationRoot configuration = configurationBuilder
            .AddJsonFile("appSettings.json", true, true)
            .Build();

            var timeout = configuration.GetSection("ApiConfiguration:Timeout").Value;

            var apiConfiguration = new ApiConfiguration();

            configuration.Bind("ApiConfiguration", apiConfiguration);

            Console.WriteLine($"Hello, the value for the timeout is {timeout}.");
            Console.WriteLine($"Hello, the value for the enabled is {apiConfiguration.Enabled}.");
        }
    }
}
