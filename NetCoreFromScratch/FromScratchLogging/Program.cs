﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;

namespace FromScratchLogging
{
    class Program
    {
        static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appSettings.json", true, true)
                .Build();

            //Log.Logger = new LoggerConfiguration()
            //    .ReadFrom.Configuration(configuration)
            //    .CreateLogger();
            //
            //Log.Warning("Hello from, Serilog!");
            //
            //Log.CloseAndFlush();

            var serviceCollection = new ServiceCollection()
                .AddLogging(builder => builder.AddSerilog(
                    new LoggerConfiguration()
                        .ReadFrom.Configuration(configuration)
                        .CreateLogger()))
                .BuildServiceProvider();

            var logger = serviceCollection.GetService<ILogger<Program>>();

            logger.LogWarning("Hello from, Serilog!");
            logger.LogInformation("Press Enter key to quit.");
            Console.ReadLine(); // because the logging is async the app might end before the logging is complete.
        }
    }
}
